﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParkingSystem.Models
{
    public class Plot
    {
        [Key]
        public int PlotId { get; set; }
        public string PlotName { get; set; }
        public int AreaId { get; set; }
        public int PlotStatus { get; set; }
        public int PlotCapacity { get; set; }
    }
}