﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParkingSystem.Models
{
    public class Customer
    {
        [Key]
        public int CustomerId { get; set; }
        public int CustomerStatus { get; set; }
    }
}