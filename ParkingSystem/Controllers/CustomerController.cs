﻿using ParkingSystem.Repository;
using ParkingSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ParkingSystem.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }
        //public ActionResult AddCustomer()
        //{
        //    return View();
        //}
        //[HttpPost]
        //public ActionResult AddCustomer(CustomerDetailsViewModel customerdetailviewmodel)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            CustomerDetailsRepository customerdetailrepo = new CustomerDetailsRepository();

        //            //PersonReg preg = new PersonReg();  Person p = new Person();

        //           customerdetailrepo.AddCustomer(customerdetailviewmodel);

        //            ViewBag.Message = "Records added successfully.";

        //        }

        //        return View();
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
        public ActionResult AddCustomer1()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddCustomer1(CustomerRegisterViewModel customerregisterviewmodel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    CustomerRegisterRepository customerregisterrepo = new CustomerRegisterRepository();

                    //PersonReg preg = new PersonReg();  Person p = new Person();

                    customerregisterrepo.AddCustomer(customerregisterviewmodel);

                    ViewBag.Message = "Records added successfully.";

                }

                return View();
            }
            catch
            {
                return View();
            }
        }

    }
}