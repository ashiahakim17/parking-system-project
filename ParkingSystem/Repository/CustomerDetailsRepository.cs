﻿using Dapper;
using ParkingSystem.Models;
using ParkingSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ParkingSystem.Repository
{
    public class CustomerDetailsRepository
    {
        private SqlConnection con;
        private void connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["Parking"].ConnectionString;
            con = new SqlConnection(constr);
        }
             public void AddCustomer(CustomerDetailsViewModel cust)
            {
            //Additing the employess      
            try
            {
                connection();
                con.Open();
                var Para = new DynamicParameters();
               CustomerDetails cc=new CustomerDetails();
                Para.Add("@UserName", cust.UserName);
                Para.Add("@Password", cust.Password);
                Para.Add("@RoleId", 2);
                var Para2 = new DynamicParameters();
                Para2.Add("@UserName", cust.UserName);
                con.Execute("AddNewUserRolesDetails", Para, commandType: CommandType.StoredProcedure);
               var UserId= con.Query<int>("GetUserId",Para2, commandType: CommandType.StoredProcedure).Single();
                var Para1 = new DynamicParameters();
                Para1.Add("@Name", cust.Name);
                Para1.Add("@Address", cust.Address);
                Para1.Add("@MobileNo", cust.MobileNo);
                Para1.Add("@UserId",UserId);
                Para1.Add("@CustomerStatus", 1);
                con.Execute("AddNewCustomerDetails", Para1, commandType: CommandType.StoredProcedure);

                //con.Execute("AddNewEmpDetails", objEmp, commandType: CommandType.StoredProcedure);
                //con.Close();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

    }
}
