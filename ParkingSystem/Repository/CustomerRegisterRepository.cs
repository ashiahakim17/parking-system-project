﻿using Dapper;
using ParkingSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ParkingSystem.Repository
{
    public class CustomerRegisterRepository
    {
        private SqlConnection con;
        private void connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["Parking"].ConnectionString;
            con = new SqlConnection(constr);
        }
        public void AddCustomer(CustomerRegisterViewModel cust)
        {
            //Additing the employess      
            try
            {
                connection();
                con.Open();
                var Para = new DynamicParameters();
                //CustomerDetails cc = new CustomerDetails();
                Para.Add("@PersonName", cust.Name);
                Para.Add("@MobileNo", cust.MobileNo);
                Para.Add("@Address", cust.Address);
                Para.Add("@UserName", cust.UserName);
                Para.Add("@Password", cust.Password);
                Para.Add("@PersonStatus", 1);
                Para.Add("@CustomerStatus", 1);
                
                
              
                con.Execute("EnterNewPersonDetails4", Para, commandType: CommandType.StoredProcedure);
               

                //con.Execute("AddNewEmpDetails", objEmp, commandType: CommandType.StoredProcedure);
                //con.Close();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

    }
}